from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer
from .models import Profile


class ProfileSerializer(ModelSerializer):

    class Meta:
        model = Profile
        fields = ['designation']


class UserSerializer(ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'first_name',
                  'last_name', 'email', 'is_superuser', 'profile']

    def to_representation(self, obj):
        """Move fields from profile to user representation."""
        representation = super().to_representation(obj)
        representation['username'] = representation['username'].lower()
        profile_representation = representation.pop('profile')
        for key in profile_representation:
            representation[key] = profile_representation[key]

        return representation

    def to_internal_value(self, data):
        profile_fields = [field.field_name for field in ProfileSerializer()]
        prepared_data, profile_data = {}, {}
        for key, val in data.items():
            if key in profile_fields:
                profile_data[key] = val
            else:
                prepared_data[key] = val
        prepared_data['profile'] = profile_data
        return super().to_internal_value(prepared_data)

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()

        Profile.objects.create(user=user, **profile_data)
        return user

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.email = validated_data.get('email', instance.email)
        instance.username = validated_data.get('username', instance.username)
        instance.date_joined = validated_data.get('date_joined', instance.date_joined)
        instance.save()

        if 'profile' in validated_data and validated_data['profile']:
            profile_data = validated_data.pop('profile')
            instance.profile.designation = profile_data.get('designation', instance.profile.designation)
            instance.profile.save()

        return instance

from rest_framework import routers
from django.conf.urls import url, include
from .views import UserViewSet

app_name = 'users'
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = [
    url(r'', include(router.urls))

]

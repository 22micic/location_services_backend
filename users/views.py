from rest_framework.viewsets import ModelViewSet
from .serializers import UserSerializer
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    authentication_classes = (TokenAuthentication, )
    permission_classes = [IsAuthenticated]


class LoginUser(ObtainAuthToken):
    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        user_details = User.objects.get(username=request.data['username'])
        token, created = Token.objects.get_or_create(user=user)

        data = {
            'token': token.key,
            'user_id': user_details.id,
            'username': user_details.username,
            'first_name': user_details.first_name,
            'last_name': user_details.last_name,
            'is_superuser': user_details.is_superuser,
            'designation': user_details.profile.designation
        }

        return Response(data)

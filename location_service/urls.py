from rest_framework import routers
from django.conf.urls import url, include
from .views import *

app_name = 'location_services'
router = routers.DefaultRouter()
router.register(r'cities', CitiesViewSet)
router.register(r'locations', LocationsViewSet)

urlpatterns = [
    url(r'', include(router.urls))

]

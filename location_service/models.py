from django.db import models


class City(models.Model):
    city = models.CharField(max_length=50, blank=False, null=False)

    def __str__(self):
        return self.city


class Location(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)
    address = models.CharField(max_length=100, blank=False, null=False)
    city = models.ForeignKey(City, null=False, blank=False, on_delete=models.CASCADE)
    latitude = models.DecimalField(max_digits=10, decimal_places=8, null=False, blank=False)
    longitude = models.DecimalField(max_digits=11, decimal_places=8,  null=False, blank=False)

    def __str__(self):
        return self.name

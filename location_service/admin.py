from django.contrib import admin
from .models import Location, City

# Register your models here.
admin.site.register(Location)
admin.site.register(City)

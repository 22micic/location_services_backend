from rest_framework.serializers import ModelSerializer
from .models import *


class CitySerializer(ModelSerializer):

    class Meta:
        model = City
        fields = ['id', 'city']


class LocationSerializer(ModelSerializer):
    city = CitySerializer()

    class Meta:
        model = Location
        fields = ['id', 'name', 'address', 'latitude', 'longitude', 'city', 'city_id']

    def to_representation(self, obj):
        """Move fields from profile to user representation."""
        representation = super().to_representation(obj)
        city_representation = representation.pop('city')
        for key in city_representation:
            if key == 'id':
                representation['city_' + key] = city_representation[key]
            else:
                representation[key] = city_representation[key]

        return representation

    def to_internal_value(self, data):
        city_fields = [field.field_name for field in CitySerializer()]
        prepared_data, city_data = {}, {}
        for key, val in data.items():
            if key in city_fields:
                city_data[key] = val
            else:
                prepared_data[key] = val
        prepared_data['city'] = city_data
        return prepared_data

    def create(self, validated_data):
        validated_data.pop('city')
        location = Location.objects.create(**validated_data)
        location.save()

        return location

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.address = validated_data.get('address', instance.address)
        instance.latitude = validated_data.get('latitude', instance.latitude)
        instance.longitude = validated_data.get('longitude', instance.longitude)
        instance.city_id = validated_data.get('city_id', instance.city_id)
        instance.save()

        return instance

